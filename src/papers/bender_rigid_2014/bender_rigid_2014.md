# Interactive Simulation of Rigid Body Dynamics in Computer Graphics
  [Paper](https://onlinelibrary.wiley.com/doi/abs/10.1111/cgf.12272)

---
- Authors
  - Jan Bender: Graduate School CE, TU Darmstadt, Germany
  - Kenny Erlenen: Department of Computer Science, University of Copenhagen, Denmark
  - Jeff Trinkle: Department of Computer Science, Rensselaer Polytechnic Institute, USA
---

## Introduction

![Body frame](body_frame.png "A body in Inertial frame [N] with body frame [B]")
