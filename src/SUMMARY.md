# Summary

- [Introduction](README.md)
- [Papers review](papers/README.md)
    - [Interactive Simulation of Rigid Body Dynamics in Computer Graphics](papers/bender_rigid_2014/bender_rigid_2014.md)